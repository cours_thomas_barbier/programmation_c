# Cours de Programmation C

Un cours dédié à l'aprentissage du C au niveau débutant. Contient cours et exercices sous forme de notebooks.

## Architecture du cours

Vous trouverez les ressources liées au cours dans les dossiers suivants:

- Cours_HTML: contient les TP de cours que vous pouvez ouvrir depuis votre ordinateur sans connexion internet.

- Cours_MD: contient les TP de cours que vous pouvez ouvrir directement depuis le site web.

- Correction: dossier qui contiendra les corrections des TP après qu'ils aient étés réalisés.

- ressources: dossier contenant les images du cours.

- COMPILER.md: fichier expliquant comment compiler un fichier c sur linux.


